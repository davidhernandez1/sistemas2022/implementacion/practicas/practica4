﻿DROP DATABASE IF EXISTS m1u2p4;
CREATE DATABASE IF NOT EXISTS m1u2p4;
USE m1u2p4;
CREATE TABLE empleado
(
  `nss-empleado` varchar(100),
  nombre varchar(100),
  apellido varchar(100),
  iniciales varchar(10),
  `fecha_ncto` date,
  sexo varchar(10),
  direccion varchar(100),
  salario float,
  `nombre-dpertenece` varchar(100),
  PRIMARY KEY(`nss-empleado`)
);
CREATE TABLE supervisa 
(
  `nss-empleado` varchar(100),
  `nss-supervisor`varchar(100),
  PRIMARY KEY(`nss-empleado`,`nss-supervisor`),
  UNIQUE KEY (`nss-empleado`),
  CONSTRAINT fksupervisaempleado1
  FOREIGN KEY (`nss-empleado`)
  REFERENCES empleado(`nss-empleado`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT,
  CONSTRAINT fksupervisaempleado2
  FOREIGN KEY (`nss-supervisor`)
  REFERENCES empleado(`nss-empleado`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
CREATE TABLE dependiente
(
  `nombre_dependiente` varchar(100),
  `nss-empleado` varchar(100),
  sexo varchar(10),
  `defcha_ncto` date,
  parentesco varchar(100),
  CONSTRAINT fkdependienteempleado
  FOREIGN KEY (`nss-empleado`)
  REFERENCES empleado(`nss-empleado`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
CREATE TABLE departamento
(
  `nombre-d` varchar(100),
  `numero-d` int,
  numdeempleados int,
  `nss-empleado-dirige` varchar(100),
  `fecha-inicio-jefe` date,
  PRIMARY KEY(`nombre-d`,`numero-d`),
  UNIQUE KEY (`nss-empleado-dirige`),
  CONSTRAINT fkdepartamentoempleado
  FOREIGN KEY (`nss-empleado-dirige`)
  REFERENCES empleado(`nss-empleado`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
CREATE TABLE localizaciones
(
  `nombre-d` varchar(100),
  `numero-d` int,
  `localizacion-dept` varchar(100),
  PRIMARY KEY(`nombre-d`,`numero-d`,`localizacion-dept`),
  CONSTRAINT fklocalizacionesdepartamento1
  FOREIGN KEY (`nombre-d`,`numero-d`)
  REFERENCES departamento(`nombre-d`,`numero-d`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
CREATE TABLE proyecto
(
  nombrep varchar(100),
  numerop int,
  localizacion varchar(100),
  `nombre-d controla` varchar(100) NOT NULL,
  `numero-d controla` int NOT NULL,
  PRIMARY KEY(nombrep,numerop),
  CONSTRAINT fkproyectodepartamento
  FOREIGN KEY (`nombre-d controla`,`numero-d controla`)
  REFERENCES departamento(`nombre-d`,`numero-d`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
CREATE TABLE `trabaja_en`
(
  `nss-empleado` varchar(100),
  nombrep varchar(100),
  numerop int,
  horas varchar(100),
  PRIMARY KEY(`nss-empleado`,nombrep,numerop),
  CONSTRAINT `fktrabaja_enempleado`
  FOREIGN KEY (`nss-empleado`)
  REFERENCES empleado(`nss-empleado`)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT,
  CONSTRAINT `fktrabaja_enproyecto`
  FOREIGN KEY (nombrep,numerop)
  REFERENCES proyecto(nombrep,numerop)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
ALTER TABLE empleado
  ADD COLUMN `numero-d` int,
  ADD CONSTRAINT fkEmpleadoDepartamento
      FOREIGN KEY (`nombre-dpertenece`,`numero-d`)
      REFERENCES departamento(`nombre-d`,`numero-d`)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT