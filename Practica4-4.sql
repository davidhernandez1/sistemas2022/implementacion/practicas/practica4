﻿USE m1u2p4;
INSERT INTO empleado (`nss-empleado`, nombre, apellido, iniciales, fecha_ncto, sexo, direccion, salario, `nombre-dpertenece`,`numero-d`)
  VALUES ('n3', 'maria', 'maria', 'MM', CURDATE(), 'mujer', 'calle guernica', 2550, NULL,NULL),
         ('n4', 'jose', 'jose', 'JJ', CURDATE(), 'hombre', 'calle pascual', 2550, NULL,NULL);


INSERT INTO departamento (`nombre-d`, `numero-d`, numdeempleados, `nss-empleado-dirige`, `fecha-inicio-jefe`)
  VALUES ('d1', 1, 0, 'n3', CURDATE()),
         ('d2', 1, 0, 'n4', CURDATE());


INSERT INTO empleado (`nss-empleado`, nombre, apellido, iniciales, fecha_ncto, sexo, direccion, salario, `nombre-dpertenece`, `numero-d`)
  VALUES ('n1', 'pepe', 'pepe', 'PP', CURDATE(), 'hombre', 'micasa', 2000, 'd1',1),
         ('n2','juan' ,'juan' ,'JJ', CURDATE(), 'hombre','calle cisnes', 2000,'d2',1);


INSERT INTO dependiente (nombre_dependiente, `nss-empleado`, sexo, defcha_ncto, parentesco)
  VALUES('maria', 'n3', 'hombre', CURDATE(), 'supervisora'),
        ('jose', 'n4', 'hombre', CURDATE(), 'supervisor');



INSERT INTO supervisa (`nss-empleado`, `nss-supervisor`)
  VALUES ('n3', 'n1'),
         ('n4', 'n2');
INSERT INTO localizaciones (`nombre-d`, `numero-d`, `localizacion-dept`)
  VALUES ('d1', 1, 'salamanca'),
         ('d2', 1, 'ampuero');
INSERT INTO proyecto (nombrep, numerop, localizacion, `nombre-d controla`, `numero-d controla`)
  VALUES ('p3', 1, 'alamanca', 'd1', 1),
         ('p4', 1, 'ampuero', 'd2', 1);
INSERT INTO trabaja_en (`nss-empleado`, nombrep, numerop, horas)
  VALUES ('n1', 'p3', 1, '120'),
         ('n2', 'p4', 1, '120');

SELECT * FROM empleado e;
SELECT * FROM departamento d;